---
title: "BTI7056-DB"
subtitle: "Homework 4"
author:
    - Severin Kaderli
    - Marius Schär
extra-info: true
institute: "Berner Fachhochschule"
department: "Technik und Informatik"
lecturer: "Dr. Kai Brünnler"
lang: "de-CH"
toc: false
header-includes: |
    \usepackage[]{algorithm2e}
    \usepackage{setspace}
    \doublespacing
    \renewcommand{\arraystretch}{0.7}
    \def\code#1{\texttt{#1}}
...

# Aufgabe 1
> Berechnen Sie die folgenden Relationen auf $r$ und $s$.

:Table $r$

|A|B|
|-|-|
|a|d|
|b|d|
|b|e|
|c|f|

:Table $s$

|B|C|
|-|-|
|d|g|
|e|h|

## $\Pi_{A} (r)$
|A|
|-|
|a|
|b|
|b|
|c|

## $\sigma_{A = 'b'} (r)$
|A|B|
|-|-|
|b|d|
|b|e|

\newpage

## $r \times s$
|A|r.B|s.B|C|
|-|---|---|-|
|a|d  |d  |g|
|b|d  |d  |g|
|b|e  |d  |g|
|c|f  |d  |g|
|a|d  |e  |h|
|b|d  |e  |h|
|b|e  |e  |h|
|c|f  |e  |h|

## $\sigma_{r.B = s.B} (r \times s)$
|A|r.B|s.B|C|
|-|---|---|-|
|a|d  |d  |g|
|b|d  |d  |g|
|b|e  |e  |h|

## $\Pi_{A} (r) - \Pi_{A} (\sigma_{B = 'd'} (r))$
|A|
|-|
|b|
|c|

\newpage

# Aufgabe 2
> Drücken Sie die gegebenen Abfragen in der relationalen Algebra aus.  
> $\text{employee(} \underline{\text{person\_name}} \text{, street, city)}$  
> $\text{works(} \underline{\text{person\_name}} \text{, company\_name, salary)}$  
> $\text{company(} \underline{\text{company\_name}} \text{, city)}$  
> $\text{manages(} \underline{\text{person\_name}} \text{, manager\_name)}$
 
#### 1. Finde die Namen aller Angestellten, welche für "FBC" arbeiten
$$
\Pi_{\text{person\_name}} (\sigma_{\text{company\_name} = \text{'FBC'}}
(\text{employee} \bowtie \text{works}))
$$

#### 2. Finde die Namen aller Angestellten, die *nicht* für "FBC" arbeiten
$$
\Pi_{\text{person\_name}} (\sigma_{\text{company\_name} \neq \text{'FBC'}}
(\text{employee} \bowtie \text{works}))
$$

#### 3. Finde die Namen und Wohnorte aller Angestellten, welche für "FBC" arbeiten
$$
\Pi_{\text{person\_name, city}} (\sigma_{\text{company\_name} = \text{'FBC'}}
(\text{employee} \bowtie \text{works}))
$$

#### 4. Finde die Namen und Wohnorte mit Strasse aller angestellten, welche für "FBC" arbeiten und die mehr als CHF 100'000.- verdienen
$$
\Pi_{\text{person\_name, city, street}} (\sigma_{
  \text{company\_name} = \text{'FBC'}
  \;\land\;
  \text{salary} > 100000
}
(\text{employee} \bowtie \text{works}))
$$

#### 5. Finde die Namen aller Angestellten, die in der Stadt arbeiten in der sie auch wohnen
$$
\Pi_{\text{person\_name}} (\sigma_{
  \text{company.city} = \text{employee.city}
}
(\text{employee} \bowtie \text{works} \bowtie \text{company}))
$$

\newpage

#### 6. Finde die Namen aller Angestellten, für die es einen Angestellten der "FBC" gibt, der mindestends genausoviel verdient
$$
\text{minFBCSalary} \leftarrow
\mathcal{G}_{ \code{min(}
  salary  
\code{)}}
(\Pi_{salary} (\sigma_{
  \text{company\_name} = \text{'FBC'}
}
(\text{works})
))
$$
$$
\Pi_{\text{person\_name}} (\sigma_{
  \text{salary} \geq \text{minFBCSalary}
}
(\text{employee} \bowtie \text{works}))
$$

#### 7. Finde die Namen aller Angestellten, die mehr verdienen als jeder Angestellte der "FBC"
$$
\text{maxFBCSalary} \leftarrow
\mathcal{G}_{ \code{max(}
  salary  
\code{)}}
(\Pi_{\text{salary}} (\sigma_{
  \text{company\_name} = \text{'FBC'}
}
(\text{works})
))
$$
$$
\Pi_{\text{person\_name}} (\sigma_{
  \text{salary} > \text{maxFBCSalary}
}
(\text{employee} \bowtie \text{works}))
$$

#### 8. Finde die Namen aller Angestellten, die in derselben Stadt an derselben Strasse wohnen, wie ihr Manager.
$$
\Pi_{\text{person\_name}}
(
\text{employee}
\bowtie
\text{manages}
\bowtie
\rho_{\text{manager\_name, street, city}} (\text{employee}))
$$

#### 9. Finde alle Firmen, die in jeder Stadt sind, in der auch die "FBC" ist.
$$
\Pi_{\text{company\_name}} (
  \text{company} \bowtie
  \Pi_{\text{city}} (
    \sigma_{
      \text{company\_name} = \text{'FBC'}
    } (\text{company})
  )
)
$$

\newpage

# Aufgabe 3
> Gegeben seien die Relationen $r(A,B)$ und $s(B,C)$. Die Relationen liegen in Form von Listen
> von Tupeln vor. Tupel sind Listen mit fester Länge, in unserem Falle mit Länge zwei.
> Zusätzlich zu den üblichen Funktionen `head` und `tail` gibt es auf Listen (und damit auch
> auf Tupeln) die Funktion `list const(element a, list l` die die Liste liefert, die durch
> Voranstellen von Element `a` an die Liste `l` ensteht.
> Die leere Liste heist `nil`.
> Andere Funktionen auf Listen gibt es nicht.
> 
> 1. Geben Sie einen Algorithmus an, um das kartesische Produkt der beiden Relationen zu berechnen
> 2. Geben Sie einen Algorithmus an, um den natural join der beiden Relationen zu berechnen.

## Kartesisches Produkt
```{.pseudocode .numberLines}
list cartesian (list r, list s) :
  result := nil;
  foreach rTuple in r :
    foreach sTuple in s :
      resultTuple := nil;
      resultTuple := const(head(tail(sTuple)), resultTuple);
      resultTuple := const(head(sTuple), resultTuple);
      resultTuple := const(head(tail(rTuple)), resultTuple);
      resultTuple := const(head(rTuple), resultTuple);
      result := const(resultTuple, result);
  return result;
```

\newpage

## Natural Join
```{.pseudocode .numberLines}
list natJoin (list r, list s) :
  result := nil;
  foreach rTuple in r :
    rB := head(tail(rTuple));
    foreach sTuple in s :
      sB := head(sTuple);
      if rB = sB :
        resultTuple := nil;
        resultTuple := const(head(tail(sTuple)), resultTuple);
        resultTuple := const(rB, resultTuple);
        resultTuple := const(head(rTuple), resultTuple);
        result := const(resultTuple, result);
  return result;
```

\newpage

# Aufgabe 4
> Gegeben sind jeweils zwei Ausdrücke der relationalen Algebra über den Relationen
> $r(A,B,C)$ und $t(A,B,C)$. Zwei Ausdrücke $x$ und $y$ sind äquivalent, wenn für alle Relationen
> $r$ und $t$ die Auswertung von $x$ dieselbe Relation liefert, wie die Auswertung von $y$.
> Untersuchen Sie, ob die folgenden Ausdrücke äquivalent sind und begründen Sie jeweils Ihre
> Aussage.
> 
> 1. $\sigma_{A > 10} (\Pi_{A,B} (r))$ und $\Pi_{A,B} (\sigma_{A > 10} (r))$
> 2. $\Pi_{A} (r - t)$ und $\Pi_{A} (r) - \Pi_{A} (t)$

## $\sigma_{A > 10} (\Pi_{A,B} (r)) \equiv \Pi_{A,B} (\sigma_{A > 10} (r))$
Da das Prädikat $A > 10$ auf $A$ vergleicht, spielt es keine Rolle (evt. Performance)
ob die Projektion $\Pi_{\text{A,B}}$ vor- oder nachher durchgeführt wird.

\newpage

## $\Pi_{A} (r - t) \not\equiv \Pi_{A} (r) - \Pi_{A} (t)$
Einfaches Gegenbeispiel:

:Table $r$

|A|B|C|
|-|-|-|
|a|b|c|
|a|b|d|

:Table $t$

|A|B|C|
|-|-|-|
|a|b|b|
|a|b|c|

### $\Pi_{A} (r - t) = \{(a)\}$
|A|
|-|
|a|

### $\Pi_{A} (r) - \Pi_{A} (t) = \varnothing$

|A|
|-|
